<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Jobsdetail;
use App\Models\City;
use App\Models\Department;
use App\Models\Experience;
use Illuminate\Http\Request;
use App\Authorizable;

class JobsdetailsController extends Controller
{
      use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jobsdetails = Jobsdetail::where('title', 'LIKE', "%$keyword%")
                ->orWhere('descriptions', 'LIKE', "%$keyword%")
                ->orWhere('type', 'LIKE', "%$keyword%")
                ->orWhere('experience', 'LIKE', "%$keyword%")
                ->orWhere('department', 'LIKE', "%$keyword%")
                ->orWhere('city', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $jobsdetails = Jobsdetail::latest()->paginate($perPage);
        }

        return view('admin.jobsdetails.index', compact('jobsdetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $city=City::pluck('name','id');
        $department=Department::pluck('name','id');
        $experience=Experience::pluck('experience','id');

        return view('admin.jobsdetails.create',compact('city','department','experience'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'descriptions' => 'required',
			'type' => 'required',
			'experience' => 'required',
			'department' => 'required',
			'city' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        Jobsdetail::create($requestData);

        return redirect('admin/jobsdetails')->with('flash_message', 'Jobsdetail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jobsdetail = Jobsdetail::findOrFail($id);

        return view('admin.jobsdetails.show', compact('jobsdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $city=City::pluck('name','id');
        $department=Department::pluck('name','id');
        $experience=Experience::pluck('experience','id');
        $jobsdetail = Jobsdetail::findOrFail($id);

        return view('admin.jobsdetails.edit',compact('jobsdetail','city','department','experience'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'descriptions' => 'required',
			'type' => 'required',
			'experience' => 'required',
			'department' => 'required',
			'city' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        $jobsdetail = Jobsdetail::findOrFail($id);
        $jobsdetail->update($requestData);

        return redirect('admin/jobsdetails')->with('flash_message', 'Jobsdetail updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Jobsdetail::destroy($id);

        return redirect('admin/jobsdetails')->with('flash_message', 'Jobsdetail deleted!');
    }
}
