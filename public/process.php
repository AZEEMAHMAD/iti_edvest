<?php session_start();
date_default_timezone_set('Asia/Kolkata');
include_once('includes/config.php');
include_once('includes/PHPMailerAutoload.php');

// child name

if( isset($_POST['name']) && !empty($_POST['name']))
{
    $name = $_POST['name'];
}
else
{
    $name = '';
}

// mobile
if( isset($_POST['ph_number']) && !empty($_POST['ph_number']))
{
    $ph_number = $_POST['ph_number'];
}
else
{
    $ph_number = '';
}

//email
if( isset($_POST['email']) && !empty($_POST['email']))
{
    $email = $_POST['email'];
}
else
{
    $email = '';
}

//high_qaulification
if( isset($_POST['high_qaulification']) && !empty($_POST['high_qaulification']))
{
    $high_qaulification = $_POST['high_qaulification'];
}
else
{
    $high_qaulification = '';
}


//qualification year

if( isset($_POST['qualification_year']) && !empty($_POST['qualification_year']))
{
    $qualification_year = $_POST['qualification_year'];
}
else
{
    $qualification_year = '';
}




//specialization

if( isset($_POST['specialization']) && !empty($_POST['specialization']))
{
    $specialization = $_POST['specialization'];
}
else
{
    $specialization = '';
}


//certi
if( isset($_POST['certi']) && !empty($_POST['certi']))
{
    $certi = $_POST['certi'];
}
else
{
    $certi = '';
}

//gender
if( isset($_POST['gender']) && !empty($_POST['gender']))
{
    $gender = $_POST['gender'];
}
else
{
    $gender = '';
}

//exp_yr
if( isset($_POST['exp_yr']) && !empty($_POST['exp_yr']))
{
    $exp_yr = $_POST['exp_yr'];
}
else
{
    $exp_yr = '';
}


//exp_yr
if( isset($_POST['exp_mnth']) && !empty($_POST['exp_mnth']))
{
    $exp_mnth = $_POST['exp_mnth'];
}
else
{
    $exp_mnth = '';
}

//designation
if( isset($_POST['designation']) && !empty($_POST['designation']))
{
    $designation = $_POST['designation'];
}
else
{
    $designation = '';
}



//note
if( isset($_POST['note']) && !empty($_POST['note']))
{
    $note = $_POST['note'];
}
else
{
    $note = '';
}


//sel_pro
if( isset($_POST['sel_pro']) && !empty($_POST['sel_pro']))
{
    $sel_pro = $_POST['sel_pro'];
}
else
{
    $sel_pro = '';
}


      $sql = "INSERT INTO `registration_form`(`name`,`mobile` , `email` , `highest_qualification` , `qualification_year` , `specialization`, `certification`, `experience_year`,`experience_month`, `current_designation`, `Note`,`dateofprogram`, `created_at` ) VALUES ( '".$name."' , '".$ph_number."', '".$email."' , '".$high_qaulification."' ,  '".$qualification_year."' ,  '".$specialization."' ,  '".$certi."' ,'".$exp_yr."','".$exp_mnth."' ,'".$designation."' ,'".$note."'  ,'".$sel_pro."', NOW())";

if(mysqli_query($bd, $sql))
{
    $registration_number = '';
    $id = mysqli_insert_id($bd);

     if($sel_pro == '9th and 10th May 2019')
    {
        $registration_number = 'A'.str_pad($input, 10, "0", STR_PAD_LEFT).$id;
    }
    else{
        $registration_number = 'B'.str_pad($input, 10, "0", STR_PAD_LEFT).$id;
    }



  $ty_message =
'<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Emailer</title>
    <style>

     @font-face {
    font-family: "Roboto";
    font-style: normal;
    font-weight: 400;
    src:url("https://fonts.googleapis.com/css?family=Roboto");
  }
    body{
         font-family: "Roboto";
    }
    p{
        margin:2px;
        margin-bottom:7px;
        font-family: "Roboto";
    }
    </style>

  </head>
  <body>
    <table width="600" style="margin: 0 auto; background-color:#eee;border-collapse: collapse; ">
      <tr style="border-bottom: 2px solid #1F4262; display: block; padding: 20px;">

          <td width="300"  valign="top"  colspan="3">
              <img src="http://dev.firsteconomy.com/emailer/iti/irm-logo.jpg" alt="" style="display: inline-block; width: 30%; padding: 0px;"/>
          </td>
          <td width="300"  valign="top"  colspan="3" style="text-align: right;">
              <img src="http://dev.firsteconomy.com/emailer/iti/iti-edvest-logo.png" alt="" style="display: inline-block;width: 70%;padding: 0px;"/>
          </td>
      </tr>
    </table>

    <table width="600" style="margin: 0 auto; background-color:#eee;border-collapse: collapse; ">

      <tr>

      </tr>

       <tr>
            <td style="padding: 0px 20px;text-align: left;">
                  <strong><p style="font-size: 20px;font-family: "Roboto", sans-serif; margin: 0; color:#000;font-weight: 700;">Dear <span contenteditable="true"> '.$name.'</span>,</p></strong>
            </td>
       </tr>

       <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0; color:#000;">We have received your details and registered your interest for the upcoming batch.</p>
            </td>
       </tr>

       <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0; color:#000;">Thank you for registering with us for the Young Leaders Program on Business Risk Management. </p>
            </td>
       </tr>

       <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0; color:#000;">Batch Details: <span contenteditable="true">'.$sel_pro.'</span></p>
            </td>
       </tr>

       <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0; color:#000;">Venue Details: <span>Naman Midtown, "A" wing, 21<sup>st</sup> Floor, Senapati Bapat Marg, Elphistone Road, Mumbai 400013.</span></p>
            </td>
       </tr>

       <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0; color:#000;">Timing: 9.30 am to 6.30 pm</p>
            </td>
       </tr>

       <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <strong><p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0; color:#000; font-weight: 700;">Please note the following registration no: <span contenteditable="true">'.$registration_number.'</span></p></strong>
            </td>
       </tr>

       <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <i><p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0; color:#000;font-style: italic;">Please mention this code while making the payment.</p></i>
            </td>
       </tr>

        <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0; color:#000;">For any queries please,</p>
            </td>
       </tr>

        <tr>
            <td style="padding: 0px 20px;text-align: left;line-height: 22px;">
                  <p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0px; color:#000;">Call: <a href="tel: 022-24391266" style="color: #000; text-decoration: none;">022-24391266</a></p>
            </td>
       </tr>

        <tr>
            <td style="padding: 0px 20px 0px;text-align: left;line-height: 22px;">
                  <p style="font-size: 16px;font-family: "Roboto", sans-serif; margin: 0px; color:#000;">Email: <a href="mailto: irm.edvest@itiorg.com" style="color: #000; text-decoration: none;">irm.edvest@itiorg.com</a></p>
            </td>
       </tr>

        <tr>
            <td style="padding: 10px;text-align: left;line-height: 22px;">

            </td>
       </tr>

    </table>
  </body>
</html>'
;



    $mail_ty = new PHPMailer(true);
    $mail_ty->IsSMTP(); // telling the class to use SMTP
    $mail_ty->IsHTML(true); //
    $mail_ty->SMTPAuth = true; // enable SMTP authentication
    $mail_ty->SMTPSecure = "ssl"; // sets the prefix to the servier
    $mail_ty->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
    $mail_ty->Port = 465; // set the SMTP port for the GMAIL server
    $mail_ty->Username = "assist@firsteconomy.com"; // GMAIL username
    $mail_ty->Password = "f3e6f3e6"; // GMAIL password
    $mail_ty->AddAddress($email);
    // $mail_ty->AddAddress('amiti@firsteconomy.com');
    $mail_ty->SetFrom('varsha@firsteconomy.com','ITI Edvest');
    $mail_ty->Subject = "Thank you for showing interest";
    $mail_ty->Body = $ty_message;
    try{

           $mail_ty->Send();

           $_SESSION['msg'] = 'Thankyou For Register,We Will Get Back To You Soon.';
           header('Location:form_irm.php');
        } catch(Exception $e){
            $_SESSION['emsg'] = 'Something Went Wrong.';
        }
}
else {
       $_SESSION['emsg'] = 'Something Went Wrong.';
}




// function send_sms($number,$message)
// {
//     $mobileNumber = $number;
//     $user = 'ACCOU011';
//     $key = 'd5dfa90f4dXX';
//     $message = urlencode ($message);
//     $senderid = 'INFOSM';
//     $accusage = 1;

//     $url="pt.k9media.in/submitsms.jsp?user=".$user."&key=".$key."&mobile=".$number."&senderid=".$senderid."&accusage=".$accusage."&message=".$message."";
//     // var_dump($url);die;
//     $ch = curl_init($url);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     $curl_response = curl_exec($ch);
//     curl_close($ch);

//     if($curl_response)
//     {
//         return 1;
//     }
//     else
//     {
//         return 0;
//     }
// }

?>
