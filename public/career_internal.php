<html>
<head>
	<title>IRM Open Programs| ITI EdVest</title>
  	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
  	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>

	<style type="text/css">
	
		@media only screen and (max-device-width: 1600px) and (max-device-height: 900px){
			footer {
			    bottom: 14px;
			}
		}
	</style>

</head>
<body>
	
	<div class="fluid-container inner ">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-12 padding-zero image-outer  text">
						
					<?php include_once('includes/header.php'); ?>

					<p class="outer">	
					<div class="container outer_block">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="page_top">
									<h2 class="job_title">The Glitch | Senior Graphic Designer - Design</h2>
									<h5 class="job_caption">
										GroupM | Mumbai, Maharashtra
									</h5>
									<hr>
									<p class="job_desc">
										<i class="fa fa-clock" aria-hidden="true"></i> <span>21 days</span><i class="fa fa-briefcase" aria-hidden="true"></i> <span>Full- time</span><i class="fa fa-users" aria-hidden="true"></i> <span>20 views</span>
									</p>
									<button class="btn btn-filter">Apply</button>
								</div>

								<div class="page_top">
									<h4>Job Description</h4>
									<p>
										Lorem Ipsum is simply dummy text of the printing and typesetting industry.

										Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.


									</p>
									<p><strong>Qualifications:</strong></p>
									<ul>
										<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
										<li>Lorem Ipsum is simply dummy text of the printing and .</li>
										<li>Lorem Ipsum is simply dummy text of the printing and dummy text of the printing  typesetting industry.</li>
										<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
										<li>Lorem Ipsum is simply dummy text of the printing and typesetting  dummy text of the printing dummy text of the printing  industry.</li>
										<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
									</ul>
								</div>
							</div>
						</div>
					</div>		
					</p>					
							
					
			            
					
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="inner.js"></script>
	<!-- <script type="text/javascript">
		$(document).ready(function(){
			$(".filter_block .fa-sort-down").click(function(){

			});
		});
	</script> -->
</body>
</html>
