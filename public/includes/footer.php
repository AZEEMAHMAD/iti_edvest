<div class="col-md-12 padding-zero">
	<div class="hamburger">
        <span class="icon_bar"></span>
        <span class="icon_bar"></span>
        <span class="icon_bar"></span>
     </div>
	<div class="navigation">
		<a href="index.php" class="home-icon"><img src="images/home_icons/home.png"></a>
		<a href="about.php" class="about_icon"><img src="images/home_icons/about_us.png">&nbsp;ABOUT</a>
		<a href="philosophy.php" class="philosophy_icon"><img src="images/home_icons/thinker_head.png">&nbsp;PHILOSOPHY</a>
		<a href="what-we-do.php" class="what_icon"><img src="images/home_icons/setting.png">&nbsp;WHAT WE DO</a>
		<a href="team.php" class="team_icon"><img src="images/home_icons/group.png">&nbsp;FOUNDING TEAM</a>
		<a href="social.php" class="social_icon"><img src="images/home_icons/social_impact.png">&nbsp;SOCIAL IMPACT</a>
		<a href="irm.php" class="contact_icon"><img src="images/home_icons/irm.png">&nbsp;IRM IN INDIA</a>					 

		<footer class="desktop_footer">
        	<div class="copyright_text">
        		<p class="open_pro_link">Young Leaders' Business Risk Program <a href="irm_program.php" class="btn_enroll"> Enroll Now </a> </p>
        		<p>&copy; 2018. The Investment Trust Of India Limited</p>
        		<a href="contact.php" class="btn_contact">Contact Us</a>
        	</div>  
        </footer>
	</div>					
</div>	
<footer class="mobile_footer">
	<div class="copyright_text">
		<p class="open_pro_link">Young Leaders' Business Risk Program <a href="irm_program.php" class="btn_enroll"> Enroll Now </a> </p>
		
		<a href="contact.php" class="btn_contact">Contact Us</a>
		<p>&copy; 2018. The Investment Trust Of India Limited</p>
	</div>
	
</footer>