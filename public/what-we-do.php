<html>
<head>
	<title>What We Do | ITI EdVest</title>
	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">
  	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	
	<div class="fluid-container inner what-we-do">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-6 padding-zero image-outer float-left">
						<img src="images/banners/what_we_do.jpg" style="width:100%;">
					</div>

					<div class="col-md-6 text">
						<?php include_once('includes/header.php'); ?>
						
						<p class="outer">

							<h3 class="" style="text-align:left;">What We Do</h3>
							<hr>
							<div class="tagline">
								<p><sup><i class="fa fa-quote-left" aria-hidden="true" style="font-size: 10px"></i></sup> Give Wings To Your Education Institutions - ITI EdVest is catalyzing growth through structured finance and education services <sup><i class="fa fa-quote-right" aria-hidden="true" style="font-size: 10px"></i></sup></p>
							</div>
							
							<div class="top_what_we">
								<h5 class="title_tagline">Structured Finance [Fortune Integrated Assets Finance Limited]</h5>
								<!-- <h5 style="margin:30px 0;clear: both;"><li>Structured Finance [Fortune Integrated Assets Finance Limited]</li></h5> -->
								<p>Our group NBFC provides secured and unsecured finance for :</p>
															
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<ul class="icon_outer">
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/w1.png">
										</div>
										<div class="fin_text"><span class="fin_text">Expansion or renovation of your campus</span></div>
										
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/w2.png">
										</div>
										<div class="fin_text"><span class="fin_text">Launching new courses</span></div>
										
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/w3.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Improving your working capital</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/w4.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Refinancing of existing debt</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/w5.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Investing in technology & research</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/w6.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Offering easy pay schemes to your students</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/w7.jpg">
										</div>
										<div class="fin_text">	<span class="fin_text">Setting up incubation centers, computer labs and other projects</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/w8.png">
										</div>
										<div class="fin_text"><span class="fin_text">Supporting teachers in fulfilling their professional aspirations</span></div>
										
									</li>
								</ul>
							</div>
							<div class="clearfix"></div>
							<div class="top_what_we">
								<h5 class="title_tagline">Education Services [The Investment Trust Of India Limited] </h5>

								<!-- <h5 style="margin:30px 0;clear: both;"><li>Education Services [The Investment Trust Of India Limited]</li></h5> -->

								<p>ITI Limited provides globally certified proprietary simulation games focused on niche topics of relevance to students of premier education institutions in India. Our objective is to elevate these institutions by internationalization of their campuses. All our simulation games are spread over a period of 2 - 7 days with support from Indian and Global experts. Improve your accreditation and partner with us by writing to <a href="mailto:education@itiorg.com">education@itiorg.com</a>.</p>

							</div>

						

						</p>

					
					</div>
				</div>
				<div class="clearfix"></div>
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="inner.js"></script>


</body>
</html>

