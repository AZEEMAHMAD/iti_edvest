<html>
<head>
	<title>Career| ITI EdVest</title>
  	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
  	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>

	<style type="text/css">
		.filter_block .checkbox{			 
		    padding: 5px 17px;
		}
		.filter_block .checkbox input{
			margin-right: 10px;
		}
		a{
			text-decoration: none!important;
		}
		@media only screen and (max-device-width: 1600px) and (max-device-height: 900px){
			footer {
			    bottom: 14px;
			}
		}
	</style>

</head>
<body>
	
	<div class="fluid-container inner ">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-12 padding-zero image-outer  text">
						
					<?php include_once('includes/header.php'); ?>

					<p class="outer">	
					<div class="container-fluid outer_block">
						<div class="row">
						<div class="col-md-3 col-sm-3 col-xs-12 left_career">
							<div class="filter_block">
														
								<!-- accordian -->
							<div class="panel-group" id="accordion">
				                <div class="panel panel-default">
				                    <a class="panel_head" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
				                    	<div class="panel-heading">
				                        	<h4 class="panel-title">
				                             City  <i class="fas fa-sort-down float-right"></i>
				                        	</h4>
				                   		</div>
				                   	</a>
				                    <div id="collapseOne" class="panel-collapse collapse in show">
				                        <div class="panel-body">
				                        	  <form id="city_form">
											    <div class="checkbox">
											      <label><input type="checkbox" value="">Option 1</label>
											    </div>
											    <div class="checkbox">
											      <label><input type="checkbox" value="">Option 2</label>
											    </div>
											    <div class="checkbox">
											      <label><input type="checkbox" value="">Option 3</label>
											    </div>
											  </form>                    
				                        </div>
				                    </div>
				                </div>
				                <div class="panel panel-default">
				                     <a class="panel_head" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">  
					                    <div class="panel-heading">
					                        <h4 class="panel-title">
					                           Experience  <i class="fas fa-sort-down float-right"></i>
					                        </h4>
					                    </div>
				                    </a>
				                    <div id="collapseTwo" class="panel-collapse collapse">
				                        <div class="panel-body">
				                            <form id="exp_form">
										    <div class="checkbox">
										      <label><input type="checkbox" value=""> Option 1</label>
										    </div>
										    <div class="checkbox">
										      <label><input type="checkbox" value=""> Option 2</label>
										    </div>
										    <div class="checkbox ">
										      <label><input type="checkbox" value=""> Option 3</label>
										    </div>
										  </form>
				                        </div>
				                    </div>
				                </div>
				                <div class="panel panel-default">
				                    <a class="panel_head" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> 
					                    <div class="panel-heading">
					                        <h4 class="panel-title">
					                             Field  <i class="fas fa-sort-down float-right"></i>
					                        </h4>
					                    </div>
				                    </a>
				                    <div id="collapseThree" class="panel-collapse collapse">
				                        <div class="panel-body">
				                             <form id="field_form">
											    <div class="checkbox">
											      <label><input type="checkbox" value=""> Option 1</label>
											    </div>
											    <div class="checkbox">
											      <label><input type="checkbox" value=""> Option 2</label>
											    </div>
											    <div class="checkbox">
											      <label><input type="checkbox" value=""> Option 3</label>
											    </div>
										  </form>
				                        </div>
				                    </div>
				                </div>
				               
				            </div>
								<!-- accordion end -->

							<button class="btn btn-filter"> Apply   <i class="fa fa-filter" aria-hidden="true"></i> </button>
							</div>
						</div>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<div class="right_career">
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name">Job Title</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name"> happened to Nokia?  happened to Nokia?</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>


									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name">Job Title</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name">Job Title</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name">Job Title</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name">Job Title</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name">Job Title</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name"> happened to Nokia?  happened to Nokia?</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>


									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="single_job">
											<h4 class="job_name">Job Title</h4>
											<p class="job_des"><i class="fas fa-map-marker-alt"></i> Location</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</div>		
					</p>					
							
					
			            
					
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="inner.js"></script>
	<!-- <script type="text/javascript">
		$(document).ready(function(){
			$(".filter_block .fa-sort-down").click(function(){

			});
		});
	</script> -->
</body>
</html>
