<html>
<head>
	<title>Founding Team | ITI EdVest</title>
  	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
  	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>

	<style type="text/css">
		.founding-team .text h5{
			border-bottom: 1px solid #17375e;
		    width: max-content;
		    padding-bottom: 2px;
		}
		.founding-team .logo_inner{
			    max-width: 12%;
		}
		.founding-team .our-mentors{
			border-top: 2px solid #333333;
		    padding: 15px 0px 5px 0px;
		    margin-top: 25px;
		}
		.founding-team .outer-mentors{
			padding-bottom: 20px;
		}
		@media only screen and (max-device-width: 1600px) and (max-device-height: 900px){
			footer {
			    bottom: 14px;
			}
		}
	</style>

</head>
<body>
	
	<div class="fluid-container inner founding-team">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-12 padding-zero image-outer float-left text">
						
						<?php include_once('includes/header.php'); ?>

						<p class="outer">
						<h3 class="pull-left">Founding Team</h3>
							<hr>

							<h5>Hersh Shah</h5>

              <p>Mr. Hersh Shah, a Commerce Graduate from Mumbai University, is a member of the Institute of Chartered Accountants of India (ICAI) and an alumnus of IIM-Bangalore. He has over 10 years of experience in audit and assurance, risk management and IPO advisory and has extensive knowledge about the education sector including developing strategic alliances & partnerships. Hersh was previously associated with KPMG where he conceptualized and launched a training vertical and also helped in setting up an entrepreneurship business school in Mumbai. Hersh was also selected amongst the 'Top 6' of the firm as part of the Ideas for Innovation Initiative by the CEO's Office at KPMG. He was also one of the 450 people to be selected in 2014 for a social entrepreneurship train journey across India which helped him decipher rural and semi-urban India.
							
						</p>

            <p>
              <h5>Chintan Valia</h5>

              <p>Mr. Chintan Valia is a Commerce Graduate from Mumbai University, a member of the Institute of Chartered Accountants of India and has done Post Graduate Management Program from IIM Bangalore. He has founded and set up the business of vehicle financing and currently manages a portfolio of INR 1,150 crores in a with over 100+ locations across India. Earlier he has been associated with Procter & Gamble India, Australia ASEAN India (AAI), Goldman Sachs Investment Banking division London and ICICI Bank Limited Mumbai. He has an overall experience of more than a decade in the field of financial services.            
            </p>
        </p>


        <h3 class="pull-left our-mentors">Our Mentors</h3>
            <hr>
            
            <div class="row">
            	<div class="col-md-12 outer-mentors">
	            	<div class="col-md-1" style="float: left;padding: 0px;text-align: center;">
		            	<img src="images/sudhir-valia-big.jpg" style="max-width: 100%;">
		            </div>
		            <div class="col-md-11" style="float: left;">
		            	<h5>Mr. Sudhir V. Valia</h5>
		            	<p>

		            		Mr. Sudhir V. Valia is a Member of the Institute of Chartered Accountants of India and carries more than three decades of taxation and finance experience. He has been the director of Sun Pharma since 1993 and is also on the board of Taro Pharmaceuticals Ltd. He is also the promoter of Suraksha Realty which is in the business of real estate development. He has won CNBC TV18's CFO of the Year in the Pharmaceutical and Healthcare Sectors for two consecutive years (2011 and 2012). Additionally he has also been awarded with Patent in Financial Structuring in 1995. He is actively involved in the field of social activities and he has also been awarded the Adivasi Sevak Puraskar (2008-09) by the Government of Maharashtra for his contribution towards the welfare of tribals, particularly in the field of education in his capacity as visionary and director of Krishna Vrundavan Pratishthan.         
		            	</p>
		            </div>

	            </div>
            </div>


			<div class="row">
				<div class="col-md-12 outer-mentors">
	            	<div class="col-md-1" style="float: left;padding: 0px;text-align: center;">
		            	<img src="images/dr_apporva_palkar.jpg" style="max-width: 100%;">
		            </div>
		            <div class="col-md-11" style="float: left;">
		            	<h5>Dr. Apoorva Palkar</h5>
		            	<p>
		            		Dr Apoorva Palkar is a committee member of Startup Committee of Government of Maharashtra and also a Senior Advisor to Ministry of Higher & Technical Education of Maharashtra. She is Chief Operating officer (Education) with Sakal Media group .Dr. Palkar comes with over twenty two years of work experience and has spent 18 years in higher education. She is also a Promoter Trustee of MERC Institute of Management. She holds a Bachelors in Mathematics, is an Alumnus of IIM Ahmedabad and Ph.D.(Business Management). She was the Director of Sinhgad Institute of Management and Computer Applications for about 9 years. She is recipient of Ravi J Mathai Fellow award and Academic leadership award from Higher Education Forum .

		            		<br>
							She is former President - AIMS, Chairperson of a National level test 'ATMA', President, Consortium of Management Education (COME), served as Member Senate-University of Pune. She has worked on Board of Studies, Pune University for three consecutive terms. She has served as an executive member to international accreditation body ACBSP (Accreditation Council for Business Schools Programs) and AACSB (which has 716 B-schools in 48 countries), AMDISA (Association of B-schools from SAARC Countries), AICTE and AIMA. Dr. Palkar has more than 25 research publications to her credit and served as the editor of "Samudanta-Journal for Manager", International Journal for Business. She has spearheaded work with UN ECOSOC Council that led to the first Procurement Workshop being conducted in India. She was a part of the International Federation of Agriculture Development (IFAD), Rome for economic development of the state of Meghalaya.<br><br>
		            	</p>
		            </div>
	            	
	            </div>
			</div>



					</div>

					
				</div>
				<div class="clearfix"></div>
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="inner.js"></script>


</body>
</html>
