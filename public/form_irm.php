<?php session_start();?>
<html>
<head>
	<title>IRM Open Programs| ITI EdVest</title>
	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">




	<style type="text/css">
		img{
			max-width: 100%;
		}
		.no_padding{
			padding: 0px;
		}
		.sub_btn{
			background: #dd6f56;
		    padding: 5px 30px!important;
		    border: none;
		    color: #fff;
		}
		.container.pro_logo{
			display: none;
		}

		.alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
  opacity: 1;
  transition: opacity 0.6s;
  margin-bottom: 15px;
}

.alert.success {background-color: #4CAF50;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}

	</style>
</head>
<body class="irm_pro_new">
	<div class="container pro_logo">
		<?php include_once('includes/header.php'); ?>
	</div>

	<section class="top_nav">
		<div class="container-fluid">
			<div class="col-md-12 col-sm-12 col-xs-12">


				<ul class="top_nav_list">
					<li class="dwnld"><a href="docs/ITI-brochure-corporate_final.pdf" target="_blank" class="sub_btn left_btn"><i class="fa fa-hand-o-down" aria-hidden="true"></i>  Download Brochure</a></li>
					<li><a href="tel:02224391266"><i class="fa fa-phone" aria-hidden="true"></i> +91-22-24391266</a></li>
					<li><a href="irm.edvest@itiorg.com"><i class="fa fa-envelope" aria-hidden="true"></i></i> irm.edvest@itiorg.com</a></li>
					<!-- <li><a href="https://goo.gl/forms/JVM2oARMaRYUGd842" target="_blank" class="reg_btn">Registration Form</a></li> -->
				</ul>
			</div>
		</div>
	</section>
	<a class="go_home" href="https://www.itiedvest.com/irm_pro_new.php">Go to Home</a>
	<section class="irm_about">
		<div class="container tri">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center">

			<h2 class="">Registration for Young Leaders Business Risk Program</h2>
			<p>
				You will receive an email after filling all the information below
			</p>
			<a href="images/irm/pro.png" class="btn btn-info btn_steps" data-toggle="modal" data-target="#img_form">Steps.......</a>
			</div>
			<br>
			<br>
			<div class="col-md-12 col-xs-12 col-sm-12 row">
				<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="">
					<form id="form_irm" action="process.php" method="post">
						<div class="form_block">
							<div class="msg"><?php if(!empty($_SESSION['msg'])){ echo '<div class="alert success"><strong> Info! </strong> '.$_SESSION['msg'].'</div>'; }?>
							<?php if(!empty($_SESSION['emsg'])){ echo '<div class="alert"><strong> Danger! </strong> '.$_SESSION['emsg'].'</div>'; }?></div><?php unset($_SESSION['msg']); unset($_SESSION['emsg']);?>
  							<h4 class="tit_form">Candidate Details</h4>

							<div class="form-group row">
								<label class="col-md-4 col-sm-4 col-xs-12">Name</label>
								<input type="text" id="name" name="name" class="form-control col-md-8 col-sm-8 col-xs-12" placeholder="Your Answer">
							</div>

							<div class="form-group row">
								<label class="col-md-4 col-sm-4 col-xs-12">Contact Number</label>
								<input type="tel" id="ph_number" name="ph_number" class="form-control col-md-8 col-sm-8 col-xs-12" placeholder="Your Answer">
							</div>

							<div class="form-group row">
								<label class="col-md-4 col-sm-4 col-xs-12">E-mail ID</label>
								<input type="email" id="email" name="email" class="form-control col-md-8 col-sm-8 col-xs-12" placeholder="Your Answer">
							</div>
						</div>

						<div class="form_block">
							<h4 class="tit_form">Academic & Professional Details</h4>

							<div class="form-group row">
								<label class="col-md-4  col-sm-4 col-xs-12">Highest Qualification</label>
								<input type="text" id="high_qaulification" name="high_qaulification" class="form-control col-md-8 col-sm-8 col-xs-12" placeholder="Your Answer">
							</div>

							<div class="form-group row">
								<label class="col-md-4  col-sm-4 col-xs-12">Qualification Year</label>
								<!-- <input type="number" id="quali_year" name="quali_year" class="form-control col-md-8 col-sm-8 col-xs-12" placeholder="Your Answer"> -->
								<?php $currently_selected = date('Y');
  									  // Year to start available options at
									  $earliest_year = 1950;
									  // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
									  $latest_year = date('Y');

									  print '<select class="form-control col-md-8 col-sm-8 col-xs-12" id="post_grad" name="qualification_year">';
									  // Loops over each int[year] from current year, back to the $earliest_year [1950]
									  foreach ( range( $latest_year, $earliest_year ) as $i ) {
									    // Prints the option with the next year in range.
									    print '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
									  }
									  print '</select>';
								?>
							</div>

							<div class="form-group row">
								<label class="col-md-4  col-sm-4 col-xs-12">Specialization <span class="text-small">( if any )</span></label>
								<input type="text" id="specialization" name="specialization" class="form-control col-md-8 col-sm-8 col-xs-12" placeholder="Your Answer">
							</div>

							<div class="form-group row">
								<label class="col-md-4  col-sm-4 col-xs-12">Certification <span class="text-small">( if any )</span></label>
								<input type="text" id="certi" name="certi" class="form-control col-md-8 col-sm-8 col-xs-12" placeholder="Your Answer">
							</div>

							<div class="form-group  row">
								<label class="col-md-4 col-sm-4 col-xs-12"> Work Experience</label>
								<input type="number" id="exp_yr" name="exp_yr" class="form-control col-md-3 col-sm-3 col-xs-12" placeholder="Year(s)" min="0">
								<input type="number" id="exp_mnth" name="exp_mnth" class="form-control col-md-4 offset-md-1 col-sm-4 offset-sm-1 col-xs-12" placeholder=" Month(s)" min="0">
							</div>

							<div class="form-group row">
								<label class="col-md-4  col-sm-4 col-xs-12">Current Designation <br> <span class="text-small">( for working professionals only )</span></label>
								<input type="text" id="designation" name="designation" class="form-control col-md-8 col-sm-8 col-xs-12" placeholder="Your Answer">
							</div>

							<div class="form-group row">
								<label class="col-md-4  col-sm-4 col-xs-12">Note</label>
								<textarea id="note" name="note" rows="4" class="col-md-8 col-sm-8 col-xs-12 note" value="1" placeholder="Your Answer"></textarea>
							</div>
						</div>

						<div class="form_block">
							<h4 class="tit_form">Program Selection</h4>

							<div class="form-group row">
								<label class="col-md-4 col-sm-12 col-xs-12" >Date of Program</label>
								<select class="form-control" id="sel_date" name="sel_pro">
							        <option>9th and 10th May 2019 </option>
							        <option>18th and 19th May 2019</option>
							    </select>
							</div>

						</div>

						<div class="text-center">
							<input type="submit" name="submit" class="sub_btn" value="Submit">

						</div>

					</form>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 text-center">
					<div class="img_form">
						<img src="images/irm/pro.jpg">
					</div>


					<div id="img_form" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <!-- Modal content-->
					    <div class="modal-content">
					      <div class="modal-body">
					      	  <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <img src="images/irm/pro.jpg">
					      </div>
					    </div>

					  </div>
					</div>
				</div>
			</div>
		</div>
	</section>





	<!-- <?php include_once('includes/footer.php'); ?> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<script src="inner.js"></script>

	<script type="text/javascript">
		$(window).scroll(function () {
		    var dist = $(window).scrollTop()
		    if (dist > 100) {
		        $(".top_nav").addClass("fixed_nav")
		    } else {
		        $(".top_nav").removeClass("fixed_nav")
		    }
		});
	</script>

	<script>
  // VALIDATION
	  $(document).ready(function(){
	    $('#form_irm').submit(function(e){

	     // e.preventDefault();
	      var name = $('#name').val();
	      var ph_number = $('#ph_number').val();
	      var email = $('#email').val();
	      var high_qaulification = $('#high_qaulification').val();
	      var exp_yr= $('#exp_yr').val();
	      var exp_mnth= $('#exp_mnth').val();
	      var note= $('#note').val();
	      var regEx = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	      // var sel_date= $()
	      var flag=0;



	      if(name.length < 1){
	        //$('#name').after('<p class="error">this field is required</p>');
	        $('#name').addClass('error');
	        flag++;
	      }
	      else{
		    	$('#name').removeClass('error');
		    }

	       if(ph_number.length < 1){
	        $('#ph_number').addClass('error');;
	         flag++;
	        }
	        else if((ph_number.length>10)||(ph_number.length<10)){
	                $('#ph_number').addClass('error');
	                 flag++;
	        }
	        else{
		    	$('#ph_number').removeClass('error');
		    }

	     if (email.length < 1 && regEx.test(email)) {
	          $('#email').addClass('error');
	           flag++;
	        }
	        else{
		    	 $('#email').removeClass('error');
		    }

	        if(high_qaulification.length < 1){
	        $('#high_qaulification').addClass('error');
	         flag++;
	      	}
	      	else{
		    	$('#high_qaulification').removeClass('error');
		    }


	        if(exp_yr.length < 1){
		        $('#exp_yr').addClass('error');
		        flag++;
		    }
		    else{
		    	$('#exp_yr').removeClass('error');
		    }

		    if(exp_mnth.length < 1){
		        $('#exp_mnth').addClass('error');
		        flag++;
		      }
		      else{
		      	$('#exp_mnth').removeClass('error');
		      }

		    if(note.length < 1){
		        $('#note').addClass('error');
		        flag++;
		      }
		      else{
		      	$('#note').removeClass('error');
		      }

	        if(flag>0){
	            return false;
	        }
	    });
	  });

	 setTimeout( "$('.msg').hide();", 4000);
</script>



</body>
</html>
