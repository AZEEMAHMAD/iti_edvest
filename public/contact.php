<html>
<head>
	<title>Contact Us | ITI EdVest</title>
  	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
  	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	
	<div class="fluid-container inner contact">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-6 padding-zero image-outer float-left">
						<img src="images/banners/contact.jpg" style="width:100%;">
					</div>

					<div class="col-md-6 text">
						<?php include_once('includes/header.php'); ?>
						
						<p class="outer">
							<h3 class="pull-left">Contact Us</h3>
							<hr>
							<h5>Any time inquiry desk</h5>

							<p>Phone: <span> +91-22-66214900</span></p>

							<!-- <p>Email: <span><a href="mailto:eduvest@itigroup.co.in">eduvest@itigroup.co.in</a></span></p> -->
							<p>For more information about ITI Group please visit<a href=" https://itigroup.co.in/" target="_blank"> www.itigroup.com</a></p>

              				<p class="">Request our senior management to visit your institution for your loan requirements at <a href="mailto:education@itiorg.com">education@itiorg.com</a> </p>
              				<p class="">We are looking for young and dynamic candidates with an entrepreneurial and innovative bent of mind to join our group. Please send an email to <a href="mailto:edvest.hr@itiorg.com">edvest.hr@itiorg.com</a> attaching your CV and we will revert back to you soon </p>
						
						</p>
						<br><br>
						<div class="outer">
							<h5>Registered and Corporate Office address:</h5>
							<hr>
							<p>Naman Midtown "A" Wing, 21st Floor, Senapati Bapat Marg, Elphinstone Road, Mumbai - 400 013</p>
							<p>Phone: 022-4027 3600</p>
						</div>		

						<!-- <footer>
				        	<div class="copyright_text">
				        		<p>&copy; 2018. Fortune Financial Services (India) Limited.</p>
				        	</div>
				        </footer> -->
						
					</div>
				</div>
				<div class="clearfix"></div>
				
				<?php include_once('includes/footer.php'); ?>	
			</div>
		</div>
	</div>

	<script src="inner.js"></script>


</body>
</html>



