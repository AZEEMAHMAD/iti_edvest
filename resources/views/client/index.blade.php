<!DOCTYPE html>
<html lang="en" class="no-js">
<head >
    <meta charset="utf-8">
    <title>ITI EdVest</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('logo_o_zCS_icon.ico')}}">
    <meta name="description" content="An edu-focused initiative by Fortune Financial">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cloud.typography.com/732108/729784/css/fonts.css">
    <link rel="stylesheet" href="public/scripts/outdatedBrowser.min.css">
    <link rel="stylesheet" href="public/scripts/awards.css">
    <link rel="stylesheet" href="public/scripts/mobile.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal.css">

    <!--[if lte IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link rel="stylesheet" type="text/css" href="public/scripts/ie9-and-down.min.css">
    <![endif]-->

    <!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="public/scripts/ie8-and-down.min.css">
    <![endif]-->

    <script src="public/scripts/modernizr-2.6.2.min.js"></script>
    <noscript>
        <style type="text/css">
            .menu, .newBrowser{display: none;}
            .disabledJS{display: block;}
            .c_browsers.hideBrowsers{opacity: 1;}
            .browser .statistic, .browser .download{display: block; opacity: 0; margin-left: 0; left: 0; width: 100%;}
            .browser .download a{ max-width: 210px; width: 80%;}
            .browser:hover .statistic, .browser:hover .download{opacity: 1;}


        </style>
    </noscript>

</head>
<body class="noajax  home">

<!-- <div class="menu"><a data-effect="effect" class="btnCloseNav" aria-hidden="true">Close Navigation<span class="l1"></span><span class="l2"></span><span class="l3"></span></a></div> -->
<div class="logo">
    <!-- <div class="logo_flex"><img src="images/logo/logo-01.png" class="logo1"><img src="images/logo/logo-02.png" class="img1"></div> <a style="text-decoration: none;" href="https://itigroup.co.in/" target="_blank" class="logo_text"><h3><i style="font-style: italic !important;">A Fortune Financial Services (India) Limited Initiative</i></h3></a>  -->
    <img src="images/logo_o.png" alt="">
</div>
<div id="container" class="container effect">
    <!-- <nav class="main_menu effect" role="navigation" aria-label="Site">
        <span class="close"></span>
        <ul>
            <li style="    cursor: pointer;"><a class="" href="about.php" data-title="" data-url="" href="about.php">ABOUT</a><span class="menu_overlay  active"></span></li>
            <li ><a class="btnAjax " href="philosophy.php" data-title="THE PROJECT" data-url="" href="philosophy.php">PHILOSOPHY</a><span class="menu_overlay "></span></li>
            <li ><a class="btnAjax " href="what-we-do.php" data-title="HOW TO USE IT" data-url="" href="what-we-do.php">WHAT WE DO</a><span class="menu_overlay "></span></li>
            <li ><a href="team.php" rel="external" data-track="Faqs"><span>FOUNDING TEAM</span></a><span class="menu_overlay"></span></li>
            <li ><a href="social.php" rel="external" data-track="Github"><span>SOCIAL IMPACT</span></a><span class="menu_overlay"></span></li>
            <li ><a href="contact.php" rel="external" data-track="Burocratik"><span>CONTACT US</span></a><span class="menu_overlay"></span></li>
        </ul>
    </nav> -->

    <div class="pusher">
        <!-- LOAD CONTENT -->
        <div class="loadContent current">



            <div id="content" class="home" role="main" >

                <div id="preload" aria-hidden="true">
                    <img src="public/imgs/browsers-bg.png" alt="">
                    <img src="public/imgs/operatingsystems-bg.png" alt="">
                </div>


                <div class="inner">

                    <!--  HEADER  -->
                    <header class="title" role="banner">

                        <!-- end HEADER  -->
                    </header>

                    <section class="c_browsers sorted kwicks kwicks-horizontal hideBrowsers">

                        <!-- BROWSER - Chrome -->
                        <div id="chrome" class="browser">
                            <!-- <h3 class="statistic"><span>63.6%</span> people are using <br>this browser</h3> -->
                            <div class="center">
                                <h2>
                                    <img src="images/home_icons/home.png"><!-- <span></span> --> Home
                                    <p>
                                        Delivering tailored financial solutions to catalyse growth in educational institutions
                                    </p>
                                </h2>

                                <div class="download">
                                    <a href="about.php" class="browserlink" data-track="Google Chrome">Know More</a>
                                    <!-- <h4>VERSION <span>65</span></h4> -->
                                </div>
                            </div>
                            <!-- <div class="available">
                                <h4>AVAILABLE FOR </h4>
                                <ul>
                                    <li class="windows"><span>Windows</span></li>
                                    <li class="mac"><span>Mac OS</span></li>
                                    <li class="linux"><span>Linux</span></li>
                                </ul>
                            </div> -->
                        </div>

                        <!-- BROWSER - Firefox -->
                        <div id="firefox" class="browser">
                            <!-- <h3 class="statistic"><span>13.8%</span> people are using <br>this browser</h3> -->
                            <div class="center">
                                <h2><img src="images/home_icons/thinker_head.png"><!-- <span></span> --> Philosophy
                                    <p>
                                        Driven by the objective of persistent innovation coupled with utmost professionalism
                                    </p>
                                </h2>
                                <div class="download">
                                    <a href="philosophy.php" class="browserlink" data-track="Mozilla Firefox">Know More</a>
                                    <!-- <h4>VERSION <span>59</span></h4> -->
                                </div>
                            </div>
                            <!-- <div class="available">
                                <h4>AVAILABLE FOR </h4>
                                <ul>
                                    <li class="windows"><span>Windows</span></li>
                                    <li class="mac"><span>Mac OS</span></li>
                                    <li class="linux"><span>Linux</span></li>
                                </ul>
                            </div> -->
                        </div>

                        <!-- BROWSER - Internet Explorer -->
                        <div id="internetExplorer" class="browser">
                            <!-- <h3 class="statistic"><span>4.1%</span> people are using <br>this browser</h3> -->
                            <div class="center">
                                <h2 class="edge"><img src="images/home_icons/setting.png"><!-- <span></span> --> What we do
                                    <p>
                                        Financially enabling institutions to transform into flourishing ecosystems
                                    </p>
                                </h2>
                                <div class="download">
                                    <a class="edge-btn" href="what-we-do.php" class="browserlink" data-track="Internet Explorer">Know More</a>
                                    <!-- <h4>VERSIO <span>16</span></h4> -->
                                    <!-- <h5>Available only with Windows 10</h5> -->
                                </div>
                            </div>
                            <!-- <div class="available">
                                <h4>AVAILABLE FOR </h4>
                                <ul class="single">
                                    <li class="windows"><span>Windows</span></li>
                                </ul>
                            </div> -->
                        </div>

                        <!-- BROWSER - Safari -->
                        <div id="safari" class="browser">
                            <!-- <h3 class="statistic"><span>5.5%</span> people are using <br>this browser</h3> -->
                            <div class="center">
                                <h2><img src="images/home_icons/group.png"><!-- <span></span> --> Founding Team
                                    <p>
                                        Founded by visionaries having profound academic and professional backgrounds
                                    </p>
                                </h2>
                                <div class="download">
                                    <a class="macos-btn" href="team.php" class="browserlink" data-track="Apple Safari">Know More</a>
                                    <!-- <h4>VERSION <span>11</span></h4> -->
                                    <!-- <h5>Available only with macOS</h5> -->
                                </div>
                            </div>
                            <!-- <div class="available">
                                <h4>AVAILABLE FOR </h4>
                                <ul class="single">
                                    <li class="mac"><span>Mac OS</span></li>
                                </ul>
                            </div> -->
                        </div>

                        <!-- BROWSER - Opera -->
                        <div id="opera" class="browser">
                            <!-- <h3 class="statistic"><span>2.4%</span> people are using <br>this browser</h3> -->
                            <div class="center">
                                <h2><img src="images/home_icons/social_impact.png"><!-- <span></span> --> Social Impact
                                    <p>
                                        Krishna Vrundavan Pratishthan - Enhancing quality of education in tribal schools
                                    </p>
                                </h2>
                                <div class="download">
                                    <a href="social.php" data-track="Opera" class="browserlink">Know More</a>
                                    <!-- <h4>VERSION <span>52</span></h4> -->
                                </div>
                            </div>
                            <!-- <div class="available">
                                <h4>AVAILABLE FOR </h4>
                                <ul>
                                    <li class="windows"><span>Windows</span></li>
                                    <li class="mac"><span>Mac OS</span></li>
                                    <li class="linux"><span>Linux</span></li>
                                </ul>
                            </div> -->
                        </div>

                        <!-- BROWSER - Extra -->
                        <div id="extra" class="browser">
                            <!-- <h3 class="statistic"><span>2.4%</span> people are using <br>this browser</h3> -->
                            <div class="center">
                                <h2><img src="images/home_icons/irm.png" class="irm_img_big"><!-- <span></span> --> IRM in India
                                    <p>
                                        Institute of Risk Management
                                    </p>
                                </h2>
                                <div class="download">
                                    <a href="irm.php" data-track="Opera" class="browserlink">Know More</a>
                                    <!-- <h4>VERSION <span>52</span></h4> -->
                                </div>
                            </div>
                            <!-- <div class="available">
                                <h4>AVAILABLE FOR </h4>
                                <ul>
                                    <li class="windows"><span>Windows</span></li>
                                    <li class="mac"><span>Mac OS</span></li>
                                    <li class="linux"><span>Linux</span></li>
                                </ul>
                            </div> -->
                        </div>
                        <!-- end SECTION -->
                    </section>
                    <footer class="mobile_footer">

                        <div class="copyright_text">
                            <span class="home_enroll">Young Leaders Business Risk Program <a href="irm_program.php" class="home_btn_enroll"> Enroll Now </a> </span>
                            <a href="contact.php" class="browserlink1">Contact Us</a>
                            <p>© 2018. Fortune Financial Services (India) Limited.</p>
                        </div>
                    </footer>

                    <!-- <section class="c_browsers sorted kwicks kwicks-horizontal"></section> -->

                    <!-- end .inner -->
                </div>
                <!-- end #content -->


            </div>
            <!-- end .loadContent -->
        </div>


        <!-- end .pusher -->
    </div>

    <footer class="desktop_footer">
        <div class="copyright_text">
            <span class="home_enroll">Young Leaders Business Risk Program <a href="irm_program.php" class="home_btn_enroll"> Enroll Now </a> </span>
            <p>&copy; 2018. The Investment Trust Of India Limited</p>
            <a href="contact.php" class="browserlink1">Contact Us</a>
        </div>
    </footer>

    <!-- [ Modal #1 ] -->
    <div class="modal fade" id="popup" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- <button type="button" class="close" data-dismiss="modal"><i class="icon-xs-o-md"></i></button> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="images/popup.jpg" alt="">
                </div>
            </div>
        </div>
    </div>


    <!-- end .container -->
</div>


<!-- ============= SCRIPTS ============= -->
<script src="public/scripts/jquery-1.10.1.min.js"></script>
<script src="public/scripts/outdatedBrowser.min.js"></script>
<script src="public/scripts/xDomainRequest.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>

<!--[if !IE]><!-->
<script type="text/javascript" src="public/scripts/main.min.js"></script>
<!--<![endif]-->

<!--[if gte IE 9]>
<script type="text/javascript" src="public/scripts/main.min.js"></script>
<![endif]-->
<script>
    $(window).on('load',function(){
        $('#popup').modal('show');
    });

</script>

</body>
</html>
