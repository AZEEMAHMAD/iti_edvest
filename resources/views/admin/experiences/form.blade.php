<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Experience</b></div>
        <br/>
         <div class="form-group {{ $errors->has('experience') ? 'has-error' : ''}}">
    <label for="experience" class="col-md-2 control-label">{{ 'Experience' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="experience" type="text" id="experience" value="{{ isset($experience->experience) ? $experience->experience : ''}}" required>
    {!! $errors->first('experience', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-2 control-label">{{ 'Status' }} :</label>
    <div class="col-md-8">
     <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($experience->status) && $experience->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
          <div class="form-group">
                <div class="col-md-offset-4 col-md-6">
                      <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
                </div>
          </div>
     </div>
</div>
